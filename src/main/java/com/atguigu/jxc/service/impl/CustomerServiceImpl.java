package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;
    /**
     * 客户列表分页
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @Override
    public Map<String, Object> getAllCustomerList(Integer page, Integer rows, String customerName) {
        Map<String,Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        List<Customer> customer = customerDao.getCustomer(page-1, rows, customerName);
        map.put("total",customerDao.getTotal());
        map.put("rows",customer);
        return map;
    }
    /**
     * 客户的添加或修改
     * @param customer
     * @param customerId
     * @return
     */
    @Override
    public int addOrUpdateCustomer(Customer customer, Integer customerId) {
        if (customerId != null){
            return customerDao.updateCustomer(customer,customerId);
        }
        return customerDao.insertCustomer(customer);
    }
    /**
     * 客户删除，支持批量操作
     * @param ids
     * @return
     */
    @Override
    public void deleteSupplier(String ids) {
        String[] split = ids.split(",");
        Arrays.stream(split).forEach(customerId->{
            customerDao.deleteSupplier(customerId);
        });
    }
}
