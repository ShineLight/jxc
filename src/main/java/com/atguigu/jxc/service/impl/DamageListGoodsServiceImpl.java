package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {
    @Autowired
    DamageListGoodsDao damageListGoodsDao;
    @Autowired
    UserDao userDao;

    /**
     * 保存报损单
     *
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @Override
    public void saveDamageList(DamageList damageList, String damageListGoodsStr, HttpSession httpSession) {
        User user = (User) httpSession.getAttribute("currentUser");
        damageList.setUserId(user.getUserId());
        damageListGoodsDao.saveDamageList(damageList);
        List<DamageListGoods> maps = JSON.parseObject(damageListGoodsStr, new TypeReference<List<DamageListGoods>>() {
        });
        maps.stream().forEach(damageListGoods -> {
            damageListGoods.setDamageListId(damageList.getDamageListId());
            damageListGoodsDao.saveDamageListGoods(damageListGoods);
        });
    }

    /**
     * 报损单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> getDamageList(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<DamageList> list = damageListGoodsDao.getAllList(sTime,eTime);
        list.stream().forEach(damageList -> {
            damageList.setTrueName(userDao.getUserById(damageList.getUserId()).getTrueName());
        });
        map.put("rows",list);
        return map;
    }
    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @Override
    public Map<String, Object> getAllDamageGoodsById(Integer damageListId) {
        Map<String, Object> map = new HashMap<>();
        List<DamageListGoods> damageListGoods = damageListGoodsDao.getGoodsByDamageListId(damageListId);
        map.put("rows",damageListGoods);
        return map;
    }
}
