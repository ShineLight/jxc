package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {
    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;
    @Autowired
    UserDao userDao;
    /**
     * 新增报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @Override
    public void addOverflowList(OverflowList overflowList, String overflowListGoodsStr, HttpSession httpSession) {
        User user =(User) httpSession.getAttribute("currentUser");
        overflowList.setUserId(user.getUserId());
        overflowListGoodsDao.saveOverflowList(overflowList);
        Integer overflowListId = overflowList.getOverflowListId();
        List<OverflowListGoods> maps = JSON.parseObject(overflowListGoodsStr, new TypeReference<List<OverflowListGoods>>(){});
        maps.stream().forEach(overflowListGoods ->  {
            overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
            overflowListGoodsDao.saveOverflowListGoods(overflowListGoods);
        });
    }
    /**
     * 报溢单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> getOverflowList(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowList> list = overflowListGoodsDao.getAllList(sTime,eTime);
        list.stream().forEach(overflowList -> {
            overflowList.setTrueName(userDao.getUserById(overflowList.getUserId()).getTrueName());
           });
        map.put("rows",list);
        return map;
    }
    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    @Override
    public Map<String, Object> getAllOverflowGoodsById(Integer overflowListId) {
        Map<String, Object> map = new HashMap<>();
        List<DamageListGoods> overflowListGoods = overflowListGoodsDao.getGoodsByOverflowListId(overflowListId);
        map.put("rows",overflowListGoods);
        return map;
    }
}
