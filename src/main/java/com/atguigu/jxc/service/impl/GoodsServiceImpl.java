package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.dao.SaleListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.GoodsTypeService;
import org.apache.shiro.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private GoodsTypeDao goodsTypeDao;
    @Autowired
    private SaleListGoodsDao saleListGoodsDao;

    @Autowired
    GoodsTypeService goodsTypeService;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> getListInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        List<GoodsType> goodsTypeInfo = goodsTypeDao.getGoodsTypeInfo(goodsTypeId);
        GoodsType goodsType = goodsTypeInfo.get(0);

        if (goodsType.getGoodsTypeState() == 0) {
            List listInventory = goodsDao.getListInventory((page - 1) * rows, rows, codeOrName, goodsTypeId);
            map.put("total", goodsDao.getTotal());
            map.put("rows", listInventory);
            return map;
        }
        ArrayList<Goods> allGoods = goodsTypeService.getAllGoodsByGoodsTypeId(page, rows, codeOrName, goodsTypeId);
        allGoods.stream().forEach(goods -> {
            goods.setGoodsTypeName(goodsTypeDao.getGoodsTypeName(goods.getGoodsTypeId()));
            goods.setSaleTotal((saleListGoodsDao.getSaleTotalByGoodsId(goods.getGoodsId())) == null ? 0 : (saleListGoodsDao.getSaleTotalByGoodsId(goods.getGoodsId()))
            );
        });
        map.put("total", goodsDao.getTotal());
        map.put("rows", allGoods);
        return map;
    }

    /**
     * 分页查询商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> getAllGoodsInfo(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        List<GoodsType> goodsTypeInfo = goodsTypeDao.getGoodsTypeInfo(goodsTypeId);
        GoodsType goodsType = goodsTypeInfo.get(0);
        if (goodsType.getGoodsTypeState() == 0) {
            List listInventory = goodsDao.getAllGoodsInfo((page - 1) * rows, rows, goodsName, goodsTypeId);
            map.put("total", goodsDao.getTotal());
            map.put("rows", listInventory);
            return map;
        }
        ArrayList<Goods> allGoods = goodsTypeService.getAllGoodsByGoodsTypeId(page, rows, goodsName, goodsTypeId);
        allGoods.stream().forEach(goods -> {
            goods.setGoodsTypeName(goodsTypeDao.getGoodsTypeName(goods.getGoodsTypeId()));
            goods.setSaleTotal((saleListGoodsDao.getSaleTotalByGoodsId(goods.getGoodsId())) == null ? 0 : (saleListGoodsDao.getSaleTotalByGoodsId(goods.getGoodsId()))
            );
        });

        map.put("total", goodsDao.getTotal());
        map.put("rows", allGoods);
        return map;
    }

    @Override
    public int removeGoods(Integer goodsId) {
        Goods goodsById = goodsDao.getGoodsById(goodsId);
        if (goodsById.getState() != 0) {
            return goodsById.getState();
        }
        goodsDao.removeGoodsById(goodsId);
        return 0;
    }

    /**
     * 分页查询无库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        List<Goods> noInventoryQuantity = goodsDao.getNoInventoryQuantity((page - 1) * rows, rows, nameOrCode);
        map.put("total", goodsDao.getNoTotal());
        return getStringObjectMap(map, noInventoryQuantity);
    }
    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @Override
    public Map<String, Object> getInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        List<Goods> inventoryQuantity = goodsDao.getInventoryQuantity((page - 1) * rows, rows, nameOrCode);
        map.put("total", goodsDao.getInTotal());
        return getStringObjectMap(map, inventoryQuantity);
    }
    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @Override
    public void addStockOrUpdateValue(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice) {
        goodsDao.updateStock(goodsId,inventoryQuantity,purchasingPrice);
    }
    /**
     * 删除商品库存
     *
     * @param goodsId 商品ID
     * @return
     */
    @Override
    public Integer removeStock(Integer goodsId) {
        Goods goodsById = goodsDao.getGoodsById(goodsId);
        Integer state = goodsById.getState();
        if (state != 0 ){
            return state;
        }
        goodsDao.removeStock(goodsId);
        return 0;
    }
    /**
     * 查询库存报警商品信息
     * @return
     */
    @Override
    public Map<String, Object> selectOutOfStock() {
        Map<String,Object> map = new HashMap<>();
        List<Goods> allGoods = goodsDao.getAllGoods();
        List<Goods> collect = allGoods.stream().filter(goods -> {
            int i = goods.getInventoryQuantity().compareTo(goods.getMinNum());
            if (i < 0) {
                return true;
            }
            return false;
        }).collect(Collectors.toList());
        map.put("rows",collect);
        return map;
    }

    /**
     * 封装结果
     * @param map
     * @param noInventoryQuantity
     * @return
     */
    private Map<String, Object> getStringObjectMap(Map<String, Object> map, List<Goods> noInventoryQuantity) {
        noInventoryQuantity.stream().forEach(goods -> {
            goods.setGoodsTypeName(goodsTypeDao.getGoodsTypeName(goods.getGoodsTypeId()));
            goods.setSaleTotal((saleListGoodsDao.getSaleTotalByGoodsId(goods.getGoodsId())) == null ? 0 : (saleListGoodsDao.getSaleTotalByGoodsId(goods.getGoodsId()))
            );
        });
        map.put("rows", noInventoryQuantity);
        return map;
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods   商品信息实体
     * @param goodsId
     * @return
     */
    @Override
    public int addOrUpdateGoods(Goods goods, Integer goodsId) {
        goods.setInventoryQuantity(0);
        goods.setState(0);
        if (goodsId != null) {
            return goodsDao.updateGoods(goods, goodsId);
        }
        return goodsDao.insertGoods(goods);
    }


}


