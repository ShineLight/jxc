package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.GoodsTypeService;
import com.atguigu.jxc.service.LogService;
import org.apache.shiro.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @description
 */
@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private LogService logService;
    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public  ArrayList<Object> loadGoodsType() {
        logService.save(new Log(Log.SELECT_ACTION, "查询商品类别信息"));


        return this.getAllGoodsType(-1); // 根节点默认从-1开始
    }
    /**
     * 新增分类
     * @param goodsTypeName
     * @param pId
     * @return
     */
    @Override
    public void addNewCategory(String goodsTypeName, Integer pId) {
        List<GoodsType> goodsTypeInfo = goodsTypeDao.getGoodsTypeInfo(pId);
        GoodsType goodsType = goodsTypeInfo.get(0);
        if (goodsType.getGoodsTypeState() == 0){
            goodsType.setGoodsTypeState(1);
            goodsTypeDao.updateGoodsTypeState(goodsType);
        }
        goodsTypeDao.addNewCategory(goodsTypeName,pId);
    }
    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    @Override
    public void removeCategory(Integer goodsTypeId) {
        List<GoodsType> goodsTypeInfo = goodsTypeDao.getGoodsTypeInfo(goodsTypeId);
        GoodsType goodsType = goodsTypeInfo.get(0);
        List<GoodsType> goodsTypeInfo1 = goodsTypeDao.getGoodsTypeInfo(goodsType.getPId());
        GoodsType goodsType1 = goodsTypeInfo1.get(0);
        goodsTypeDao.removeCategroy(goodsTypeId);
        List<GoodsType> allGoodsTypeByParentId = goodsTypeDao.getAllGoodsTypeByParentId(goodsType.getPId());
        if (allGoodsTypeByParentId.size() == 0){
            goodsType1.setGoodsTypeState(0);
            goodsTypeDao.updateGoodsTypeState(goodsType1);
        }

    }

    /**
     * 递归查询所有商品类别
     *
     * @return
     */
    public ArrayList<Object> getAllGoodsType(Integer parentId){

        ArrayList<Object> array = this.getGoodSTypeByParentId(parentId);

        for(int i = 0;i < array.size();i++){

            HashMap obj = (HashMap) array.get(i);

            if(obj.get("state").equals("open")){// 如果是叶子节点，不再递归

            }else{// 如果是根节点，继续递归查询
                obj.put("children", this.getAllGoodsType(Integer.parseInt(obj.get("id").toString())));
            }

        }

        return array;
    }

    /**
     * 根据父ID获取所有子商品类别
     * @return
     */
    public ArrayList<Object> getGoodSTypeByParentId(Integer parentId){

        ArrayList<Object> array = new ArrayList<>();

        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(parentId);

        System.out.println("goodsTypeList" + goodsTypeList);
        //遍历商品类别
        for(GoodsType goodsType : goodsTypeList){

            HashMap obj = new HashMap<String, Object>();

            obj.put("id", goodsType.getGoodsTypeId());
            obj.put("text", goodsType.getGoodsTypeName());

            if(goodsType.getGoodsTypeState() == 1){
                obj.put("state", "closed");

            }else{
                obj.put("state", "open");
            }

            obj.put("iconCls", "goods-type");

            HashMap<String, Object> attributes = new HashMap<>();
            attributes.put("state", goodsType.getGoodsTypeState());
            obj.put("attributes", attributes);

            array.add(obj);

        }

        return array;
    }
    /**
     * 通过商品类型ID查询所有子商品
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    public ArrayList<Goods> getAllGoodsByGoodsTypeId(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        ArrayList<Goods> arrayList = new ArrayList<>();
        Integer finallpage = page == 0 ? 1 : page;
        //获取此分类下所有分类的Map集合
        ArrayList<Object> typeIdMapByGoodsTypeId = getTypeIdMapByGoodsTypeId(goodsTypeId);
        //获取该分类下所有商品的集合
        typeIdMapByGoodsTypeId.stream().forEach(type->{
            List<GoodsType> result = new ArrayList<>();
            if (type instanceof ArrayList<?>) {
                for (Object o : (List<?>) type) {
                    result.add(GoodsType.class.cast(o));
                }
            }
            result.stream().forEach(goodsType -> {
                List listInventory = goodsDao.getListInventory((finallpage - 1) * rows, rows, codeOrName, goodsType.getGoodsTypeId());
                listInventory.stream().forEach(goodsList->{
                    arrayList.add((Goods) goodsList);
                });
            });
        });
        return arrayList;
    }

    /**
     * 根据用户传入的id获取所有子商品的Obj集合
     * @param goodsTypeId
     */
    public ArrayList<Object> getTypeIdMapByGoodsTypeId(Integer goodsTypeId) {
        Map<String,Object> map = new HashMap<>();
        int i = goodsTypeId == null ? -1 : goodsTypeId;
        this.findAllTypeId(i,map);
        Map<String, Object> map1 = this.removeEmpty(map);
        ArrayList<Object> objects = new ArrayList<>(map1.values());
        return objects;
    }

    /**
     * 根据父级Id获取子集列表
     * @param id
     * @param map
     * @return
     */
    public List findAllTypeId(Integer id, Map<String,Object> map){
        List<GoodsType> allGoodsTypeByParentId = goodsTypeDao.getAllGoodsTypeByParentId(id);
        map.put(id.toString(),allGoodsTypeByParentId);
        List<GoodsType> collect = allGoodsTypeByParentId.stream().filter(goodsType -> {
            Boolean result = CollectionUtils.isEmpty(this.findAllTypeId(goodsType.getGoodsTypeId(),map));
            return result;
        }).collect(Collectors.toList());

        return collect;
    }

    /**
     * 移除map中value为空的key
     * @param map
     * @return
     */
    public Map<String, Object> removeEmpty(Map<String, Object> map) {
        return map.entrySet().stream()
                .filter(entry -> entry.getValue() != null)
                .filter(entry -> !(entry.getValue() instanceof String) || !StringUtils.isEmpty(entry.getValue()))
                .filter(entry -> !(entry.getValue() instanceof List) || !CollectionUtils.isEmpty((List) entry.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

}
