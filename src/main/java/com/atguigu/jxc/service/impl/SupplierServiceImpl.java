package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    SupplierDao supplierDao;
    /**
     * 分页查询供应商
     * @param page 当前页数
     * @param rows 每页显示记录数
     * @param supplierName
     * @return 供应商名字
     */
    @Override
    public Map<String, Object> getSupplier(Integer page, Integer rows, String supplierName) {
        Map<String,Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        List<Supplier> supplier = supplierDao.getSupplier((page-1)*rows, rows, supplierName);
        map.put("total",supplierDao.getTotal());
        map.put("rows",supplier);
        return map;
    }
    /**
     * 供应商查询或修改
     * @param supplier
     * @param supplierId
     * @return
     */
    @Override
    public int addOrUpdateSupplier(Supplier supplier, Integer supplierId) {
        if (supplierId != null){
            return supplierDao.updateSupplier(supplier,supplierId);
        }
        return supplierDao.insertSupplier(supplier);
    }
    /**
     * 删除供应商，支持批量删除
     * @param ids
     * @return
     */
    @Override
    public void deleteSupplier(String ids) {
        String[] split = ids.split(",");
        Arrays.stream(split).forEach(supplierId->{
            supplierDao.deleteSupplier(supplierId);
        });
    }


}
