package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.shiro.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description
 */
public interface GoodsTypeService {
    ArrayList<Object> loadGoodsType();
    /**
     * 新增分类
     * @param goodsTypeName
     * @param pId
     * @return
     */
    void addNewCategory(String goodsTypeName, Integer pId);
    /**
     * 通过商品类型ID查询所有子商品
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    public ArrayList<Goods> getAllGoodsByGoodsTypeId(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    /**
     * 根据用户传入的id获取所有子商品的Obj集合
     * @param goodsTypeId
     */
    public ArrayList<Object> getTypeIdMapByGoodsTypeId(Integer goodsTypeId);

    /**
     * 根据父级Id获取子集列表
     * @param id
     * @param map
     * @return
     */
    public List findAllTypeId(Integer id, Map<String,Object> map);

    /**
     * 移除map中value为空的key
     * @param map
     * @return
     */
    public Map<String, Object> removeEmpty(Map<String, Object> map);
    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    void removeCategory(Integer goodsTypeId);
}
