package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface OverflowListGoodsService {
    /**
     * 新增报溢单
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    void addOverflowList(OverflowList overflowList, String overflowListGoodsStr, HttpSession httpSession);
    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    Map<String, Object> getOverflowList(String sTime, String eTime);
    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    Map<String, Object> getAllOverflowGoodsById(Integer overflowListId);
}
