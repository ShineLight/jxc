package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    /**
     * 分页查询供应商
     * @param page 当前页数
     * @param rows 每页显示记录数
     * @param supplierName
     * @return 供应商名字
     */
    Map<String, Object> getSupplier(Integer page, Integer rows, String supplierName);
    /**
     * 供应商查询或修改
     * @param supplier
     * @param supplierId
     * @return
     */
    int addOrUpdateSupplier(Supplier supplier, Integer supplierId);
    /**
     * 删除供应商，支持批量删除
     * @param ids
     * @return
     */
    void deleteSupplier(String ids);
}
