package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {
    /**
     * 客户列表分页
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    Map<String, Object> getAllCustomerList(Integer page, Integer rows,String customerName);
    /**
     * 客户的添加或修改
     * @param customer
     * @param customerId
     * @return
     */
    int addOrUpdateCustomer(Customer customer, Integer customerId);
    /**
     * 客户删除，支持批量操作
     * @param ids
     * @return
     */
    void deleteSupplier(String ids);
}
