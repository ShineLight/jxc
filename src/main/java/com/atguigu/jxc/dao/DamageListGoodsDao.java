package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListGoodsDao {
    /**
     * 保存报损单
     * @param damageList
     */
    Integer saveDamageList(DamageList damageList);

    /**
     * 保存报损商品
     * @param damageListGoods
     */
    void saveDamageListGoods(@Param("damageListGoods")DamageListGoods damageListGoods);

    /**
     * 查询报损单
     * @param sTime
     * @param eTime
     * @return
     */
    List<DamageList> getAllList(@Param("sTime")String sTime,@Param("eTime")String eTime);

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    List<DamageListGoods> getGoodsByDamageListId(@Param("damageListId") Integer damageListId);
}
