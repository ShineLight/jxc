package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {
    /**
     * 客户列表分页
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    List<Customer> getCustomer(@Param("page") Integer page, @Param("rows") Integer rows, @Param("customerName") String customerName);
    /**
     * 查询记录总数
     * @return
     */
    Object getTotal();
    /**
     * 客户更改
     * @param customer
     * @param customerId
     * @return
     */
    int updateCustomer(@Param("customer") Customer customer,@Param("customerId") Integer customerId);
    /**
     * 客户新增
     * @param customer
     * @return
     */
    int insertCustomer(@Param("customer") Customer customer);
    /**
     * 客户删除，支持批量操作
     * @param ids
     * @return
     */
    void deleteSupplier(@Param("customerId") String customerId);
}
