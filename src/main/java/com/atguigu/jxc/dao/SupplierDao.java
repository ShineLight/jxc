package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {
    /**
     * 分页查询供应商
     * @param page 当前页数
     * @param rows 每页显示记录数
     * @param supplierName
     * @return 供应商名字
     */
    List<Supplier> getSupplier(@Param("page") Integer page,@Param("rows") Integer rows,@Param("supplierName") String supplierName);
    /**
     * 供应商新增
     * @param supplier
     * @return
     */
    int insertSupplier(@Param("supplier") Supplier supplier);
    /**
     * 供应商更改
     * @param supplier
     * @param supplierId
     * @return
     */
    int updateSupplier(@Param("supplier") Supplier supplier,@Param("supplierId") Integer supplierId);
    /**
     * 删除供应商，支持批量删除
     * @param supplierId
     * @return
     */
    void deleteSupplier(@Param("supplierId") String supplierId);
    /**
     * 查询记录总数
     * @return
     */
     String getTotal();
}
