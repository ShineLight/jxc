package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    String getGoodsTypeName(@Param("goodsTypeId") Integer goodsTypeId);

    /**
     * 新增分类
     * @param goodsTypeName
     * @param pId
     * @return
     */
    void addNewCategory(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId);

    /**
     * 根据TypeId查询Type对象
     * @param goodsTypeId
     * @return
     */
    List<GoodsType> getGoodsTypeInfo(@Param("goodsTypeId") Integer goodsTypeId);
    /**
     * 删除分类
     * @param goodsTypeId
     * @return
     */
    void removeCategroy(@Param("goodsTypeId") Integer goodsTypeId);
}
