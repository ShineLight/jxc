package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {

    String getMaxCode();
    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    List getListInventory(@Param("page") Integer page,@Param("rows")Integer rows,@Param("codeOrName")String codeOrName,@Param("goodsTypeId")Integer goodsTypeId);

    /**
     * 查询记录总数
     * @return
     */
    String getTotal();
    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    List getAllGoodsInfo(@Param("page") Integer page,@Param("rows")Integer rows,@Param("goodsName")String goodsName,@Param("goodsTypeId")Integer goodsTypeId);

    /**
     * 更新商品
     * @param goods
     * @param goodsId
     */
    int updateGoods(@Param("goods")Goods goods, @Param("goodsId") Integer goodsId);

    /**
     * 新增商品
     * @param goods
     */
    int insertGoods(@Param("goods") Goods goods);

    /**
     * 修改商品
     * @param goodsId
     * @return
     */
    Goods getGoodsById(@Param("goodsId") Integer goodsId);

    /**
     * 删除商品
     * @param goodsById
     */
    void removeGoodsById(@Param("goodsId")Integer goodsById);
    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    List<Goods> getNoInventoryQuantity(@Param("page") Integer page,@Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    /**
     * 获取库存不足的商品数量
     * @return
     */
    Object getNoTotal();
    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    List<Goods> getInventoryQuantity(@Param("page") Integer page,@Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    /**
     * 获取有库存的商品数量
     * @return
     */
    Object getInTotal();

    /**
     * 更新库存及成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     */
    void updateStock(@Param("goodsId") Integer goodsId,@Param("inventoryQuantity") Integer inventoryQuantity,@Param("purchasingPrice") Double purchasingPrice);

    int removeStock(@Param("goodsId") Integer goodsId);

    /**
     * 遍历商品
     * @return
     */
    List<Goods> getAllGoods();
}
