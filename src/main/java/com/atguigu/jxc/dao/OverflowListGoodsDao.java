package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowListGoodsDao {
    /**
     * 保存报溢单
     * @param overflowList
     */
    void saveOverflowList(OverflowList overflowList);

    /**
     * 保存报溢商品
     * @param overflowListGoods
     */
    void saveOverflowListGoods(@Param("overflowListGoods") OverflowListGoods overflowListGoods);

    /**
     * 查询报溢单
     * @param sTime
     * @param eTime
     * @return
     */
    List<OverflowList> getAllList(@Param("sTime")String sTime,@Param("eTime")String eTime);

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    List<DamageListGoods> getGoodsByOverflowListId(@Param("overflowListId")Integer overflowListId);
}
