package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 商品信息Controller
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/listInventory")
    public Map<String, Object> getListInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = goodsService.getListInventory(page, rows, codeOrName, goodsTypeId);
        return map;
    }

    /**
     * 分页查询商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> getAllGoodsInfo(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String, Object> map = goodsService.getAllGoodsInfo(page, rows, goodsName, goodsTypeId);
        return map;
    }

    /**
     * 生成商品编码
     *
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods   商品信息实体
     * @param goodsId
     * @return
     */
    @PostMapping("/save")
    public ServiceVO addOrUpdateGoods(Goods goods, Integer goodsId) {
        goodsService.addOrUpdateGoods(goods, goodsId);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除商品信息
     *
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO removeGoods(Integer goodsId) {
        int i = goodsService.removeGoods(goodsId);
        return getServiceVO(i);
    }

    /**
     * 分页查询无库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getNoInventoryQuantity")
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = goodsService.getNoInventoryQuantity(page, rows, nameOrCode);
        return map;
    }

    /**
     * 分页查询有库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getHasInventoryQuantity")
    public Map<String, Object> getInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = goodsService.getInventoryQuantity(page, rows, nameOrCode);
        return map;
    }

    /**
     * 添加商品期初库存
     *
     * @param goodsId           商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice   成本价
     * @return
     */
    @PostMapping("/saveStock")
    public ServiceVO addStockOrUpdateValue(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice) {
        goodsService.addStockOrUpdateValue(goodsId, inventoryQuantity, purchasingPrice);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除商品库存
     *
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/deleteStock")
    public ServiceVO removeStock(Integer goodsId) {
        Integer integer = goodsService.removeStock(goodsId);
        return getServiceVO(integer);
    }

    /**
     * 订单返回类型
     * @param integer
     * @return
     */
    private ServiceVO getServiceVO(Integer integer) {
        if (integer == 1) {
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
        }
        if (integer == 2) {
            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
    /**
     * 查询库存报警商品信息
     * @return
     */
    @PostMapping("/listAlarm")
    public Map<String,Object> selectOutOfStock(){
        Map<String,Object> map = goodsService.selectOutOfStock();
        return map;
    }

}
