package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * 客户列表分页
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> getAllCustomerList(Integer page, Integer rows,String customerName){
        Map<String, Object> allCustomerList = customerService.getAllCustomerList(page, rows, customerName);
        return allCustomerList;
    }

    /**
     * 客户的添加或修改
     * @param customer
     * @param customerId
     * @return
     */
    @PostMapping("/save")
    public ServiceVO addOrUpdateCustomer(Customer customer,Integer customerId){
        System.out.println(customer);
        int addOrUpdateCustomer = customerService.addOrUpdateCustomer(customer,customerId);
        if (addOrUpdateCustomer >0){
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
        return new ServiceVO<>(ErrorCode.TIME_OUT_CODE,ErrorCode.TIME_OUT_MESS);
    }

    /**
     * 客户删除，支持批量操作
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO deleteSupplier(String ids){
        customerService.deleteSupplier(ids);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
