package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {
    @Autowired
    private DamageListGoodsService damageListGoodsService;

    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveDamageList(DamageList damageList, String damageListGoodsStr, HttpSession httpSession) {
        damageListGoodsService.saveDamageList(damageList,damageListGoodsStr,httpSession);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 报损单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> getDamageList(String sTime,String eTime){
        Map<String,Object> map = damageListGoodsService.getDamageList(sTime,eTime);
        return map;
    }

    /**
     * 查询报损单商品信息
     * @param damageListId
     * @return
     */
    @PostMapping("/goodsList")
    public Map<String,Object> getAllDamageGoodsById(Integer damageListId){
        Map<String,Object> map = damageListGoodsService.getAllDamageGoodsById(damageListId);
        return map;
    }
}
