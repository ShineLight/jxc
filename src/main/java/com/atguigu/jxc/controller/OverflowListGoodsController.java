package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {
    @Autowired
    private OverflowListGoodsService overflowListGoodsService;

    /**
     * 新增报溢单
     *
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @PostMapping("/save")
    public ServiceVO addOverflowList(OverflowList overflowList, String overflowListGoodsStr, HttpSession httpSession) {
        overflowListGoodsService.addOverflowList(overflowList, overflowListGoodsStr, httpSession);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> getOverflowList(String sTime, String eTime) {
        Map<String, Object> map = overflowListGoodsService.getOverflowList(sTime, eTime);
        return map;
    }

    /**
     * 查询报溢单商品信息
     * @param overflowListId
     * @return
     */
    @PostMapping("/goodsList")
    public Map<String,Object> getAllOverflowGoodsById(Integer overflowListId){
        Map<String,Object> map = overflowListGoodsService.getAllOverflowGoodsById(overflowListId);
        return map;
    }
}
