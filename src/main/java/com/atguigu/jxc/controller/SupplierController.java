package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/supplier")
public class SupplierController {
    @Autowired
    private SupplierService supplierService;

    /**
     * 分页查询供应商
     * @param page 当前页数
     * @param rows 每页显示记录数
     * @param supplierName
     * @return 供应商名字
     */
    @PostMapping("/list")
    public Map<String,Object> getSupplier(Integer page,Integer rows,String supplierName){
        Map<String,Object> map = supplierService.getSupplier(page,rows,supplierName);
        return map;
    }

    /**
     * 供应商新增或修改
     * @param supplier
     * @param supplierId
     * @return
     */
    @PostMapping("/save")
    public ServiceVO addOrUpdateSupplier(Supplier supplier,Integer supplierId){
        int updateSupplier = supplierService.addOrUpdateSupplier(supplier, supplierId);
        if (updateSupplier >0){
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
        return new ServiceVO<>(ErrorCode.TIME_OUT_CODE,ErrorCode.TIME_OUT_MESS);
    }

    /**
     * 删除供应商，支持批量删除
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO deleteSupplier(String ids){
       supplierService.deleteSupplier(ids);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
}
